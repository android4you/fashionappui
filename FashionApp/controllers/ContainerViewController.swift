//
//  ContainerViewController.swift
//  FashionApp
//
//  Created by Manu Aravind on 14/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    
    var menuController : MenuViewController!
    var centerController : UIViewController!
    var isExpanded = false

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        configureHomeController()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
   
       
       override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
           return .slide
       }
       
       override var prefersStatusBarHidden: Bool {
           return isExpanded
       }
    
    func animateStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
    }
    
    func configureHomeController() {
        let homeController  = HomeViewController()
        homeController.delegate = self
        centerController = UINavigationController(rootViewController: homeController)
        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
    }
    
    func configureMenuController() {
        
        if menuController == nil {
            menuController = MenuViewController()
            menuController.delegate = self
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
        }
        
    }
    
    func showMenuController(shouldExpand : Bool, index: Int){
    
        if shouldExpand {
         UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                           self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
                       }, completion: nil)
        }else{
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                         self.centerController.view.frame.origin.x = 0
                     }) { (_) in
                       /// guard let menuIndex = index else {return}
                   
                        self.didSelectMenuOption(index: index)
                     }
        }
        animateStatusBar()
    }
    
    
    func didSelectMenuOption(index: Int){
        switch index{
        case 0:
            print("one")
            case 1:
            print("two")
            case 2:
               let controller = BrandsViewController()
                       let navigationController = UINavigationController(rootViewController: controller)
                       navigationController.modalPresentationStyle = .overFullScreen
                       present(navigationController, animated: true, completion: nil)
            case 3:
            print("four")
              let controller = ProductViewController()
              let navigationController = UINavigationController(rootViewController: controller)
              navigationController.modalPresentationStyle = .overFullScreen
              present(navigationController, animated: true, completion: nil)
              
        default:
            print("ttt")
        }
        
    }

}


extension ContainerViewController : HomeViewControllerDelegate {
    func handleMenuToggle(index: Int) {
       
        if(!isExpanded){
            configureMenuController()
        }
       
        isExpanded = !isExpanded
        
       showMenuController(shouldExpand: isExpanded, index: index)
    }
    
    
}
