//
//  ProductViewController.swift
//  FashionApp
//
//  Created by Manu Aravind on 15/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController, NetworkManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    let cellId = "ProductCollectionViewCell"
    
    @IBOutlet weak var productCollectionView: UICollectionView!
    let rest : NetworkManager = NetworkManager()
    var viewActivity = ProgressUtils()
    var arrayofData:[ProductModel] = [ProductModel]()
    @IBOutlet weak var backButton: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        rest.initwith()
        rest.restDelegate = self
        viewActivity.showActivityIndicator(self.view);
        rest.getProducts()
        
        productCollectionView.register(UINib.init(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
    }
    
    func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = "Products"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: "backicon.png")?.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.red), style: .plain, target: self, action: #selector(handleToggle))
    }
    
    @objc func handleToggle() {
        dismiss(animated: true, completion: nil)
    }
    
    func ApiResponsegetArrived(_ Response:AnyObject, isType: String){
        viewActivity.hideActivityIndicator()
        if(isType == ActivityConstants.INT_PRODUCT){
            if let detailsjson = Response as? [[String: AnyObject]] {
                for model in detailsjson {
                    if let brand = model["brand"] as? String  {
                        print(brand)
                        let model =  ProductModel(id: (model["id"] as? Int), brand: (model["brand"] as? String) ?? "", name: (model["name"] as? String) ?? "", price: (model["price"] as? String), price_sign: (model["price_sign"] as? String ?? ""), currency: (model["currency"] as? String) ?? "", image_link: (model["image_link"] as? String) ?? "", product_link: (model["product_link"] as? String ?? ""), website_link: (model["website_link"] as? String ?? ""), description: (model["description"] as? String ?? ""), rating: (model["rating"] as? String ?? ""), category: (model["category"] as? String ?? ""), product_type: (model["product_type"] as? String ?? ""), created_at: (model["created_at"] as? String ?? ""),updated_at: (model["updated_at"] as? String ?? ""), product_api_url: (model["product_api_url"] as? String ?? ""), api_featured_image: (model["api_featured_image"] as? String ?? "") )
                        arrayofData.append(model)
                    }
                }
                DispatchQueue.main.async {
                    self.productCollectionView.reloadData()
                }
            }
            
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrayofData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ProductCollectionViewCell
        let product = arrayofData[indexPath.row]
        cell.imageView.setImageFromURl(stringImageUrl: (arrayofData[indexPath.row].image_link)!)
        cell.titleView.text = product.name
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row + 1)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.size.width/2, height: 150)
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}
