//
//  ProductCollectionViewCell.swift
//  FashionApp
//
//  Created by Manu Aravind on 16/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
