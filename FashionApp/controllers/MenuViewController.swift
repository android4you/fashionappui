//
//  MenuViewController.swift
//  Meals_db
//
//  Created by Manu Aravind on 22/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    var menuItems = [MenuItemsModel]()
    let cellId = "MenuViewCell"
    var delegate: HomeViewControllerDelegate?
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        menuItems = getMenuItems()
        tableView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
              tableView.separatorColor = UIColor.clear
              tableView.layoutIfNeeded()
    }


    func  getMenuItems() -> [MenuItemsModel] {
        var models = [MenuItemsModel]()
        models.append(MenuItemsModel(id: 1, title: "Access Wallet"))
        models.append(MenuItemsModel(id: 2, title: "Notifications"))
        models.append(MenuItemsModel(id: 3, title: "Profile"))
        models.append(MenuItemsModel(id: 4, title: "Settings"))
        models.append(MenuItemsModel(id: 5, title: "Support"))
        return models
    }

}



extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MenuViewCell
               cell.selectionStyle = .none
               let menu = menuItems[indexPath.row]
               cell.titleView.text = menu.title
               return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        print("click menu ")
        let menuOption = indexPath.row + 1
       // print(menuOption)
        delegate?.handleMenuToggle(index: menuOption)
    }
    
    
}



