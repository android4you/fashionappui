//
//  HomeViewController.swift
//  FashionApp
//
//  Created by Manu Aravind on 14/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

 var delegate : HomeViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.init(hex: "F2F3F4")
        configureNavigationBar()
    }
    
    func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = "Home"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: "ic_menu_white.png")?.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.systemPink), style: .plain, target: self, action: #selector(handleMenuToggle))
         
    }
    
    @objc func handleMenuToggle() {
        print("KKKKKKKKK")
        delegate?.handleMenuToggle(index: 0)
       }

}
