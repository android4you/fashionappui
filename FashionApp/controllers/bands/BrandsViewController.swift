//
//  BrandsViewController.swift
//  FashionApp
//
//  Created by Manu Aravind on 15/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class BrandsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let cellId = "BrandsTableViewCell"
    @IBOutlet weak var brandTableView: UITableView!
    
    func getBrandsList()  -> [String] {
        let  brandList = ["almay", "anna sui", "annabelle", "benefit",
                          "burt's bees", "butter london", "cargo cosmetics", "china glaze", "clinique", "covergirl",
                          "dalish", "dior", "dr. hauschka", "e.l.f.", "essie", "fenty", "glossier", "iman", "l'oreal",
                          "marcelle", "maybelline", "milani", "mineral fusion", "misa", "mistura", "moov", "nyx",
                          "orly", "pacifica", "physicians formula", "piggy paint", "pure anada", "revlon", "salon perfect",
                          "sante", "sinful colours", "smashbox", "stila", "suncoat", "wet n wild", "zorah"]
        return brandList
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        
        brandTableView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        brandTableView.separatorColor = UIColor.clear
        
        brandTableView.reloadData()
        
    }
    
    
    func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = "Brands"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: "backicon.png")?.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.systemPink), style: .plain, target: self, action: #selector(handleToggle))
        
    }
    
    @objc func handleToggle() {
        dismiss(animated: true, completion: nil)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getBrandsList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! BrandsTableViewCell
        cell.selectionStyle = .none
        let product = getBrandsList()[indexPath.row]
        cell.brandsTitle.text = product
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var homeVC : BrandsDetailsViewController!
        homeVC  =  BrandsDetailsViewController(nibName: "BrandsDetailsViewController", bundle: nil)
        homeVC.brand = getBrandsList()[indexPath.row]
        navigationController?.pushViewController(homeVC, animated: false)
        
    }
    
    
}
