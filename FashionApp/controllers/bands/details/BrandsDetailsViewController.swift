//
//  BrandsDetailsViewController.swift
//  FashionApp
//
//  Created by Manu Aravind on 16/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class BrandsDetailsViewController: UIViewController, NetworkManagerDelegate, UITableViewDelegate, UITableViewDataSource {
 
    var arrayofData:[ProductModel] = [ProductModel]()
    let rest : NetworkManager = NetworkManager()
    var viewActivity = ProgressUtils()
    var brand : String = ""
    let cellId = "BrandsProTableViewCell"
    
    @IBOutlet weak var productBrandTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        rest.initwith()
        rest.restDelegate = self
        viewActivity.showActivityIndicator(self.view)
        rest.getProductByBrand(_brand: brand)
        
        
        productBrandTableView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        productBrandTableView.separatorColor = UIColor.clear
       
        //tableView.rowHeight = UITableViewAutomaticDimension
       // productBrandTableView.estimatedRowHeight = 100
    }
    
    func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = "Product Brands"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: "backicon.png")?.withRenderingMode(.alwaysOriginal).withTintColor(UIColor.red), style: .plain, target: self, action: #selector(handleToggle))
    }
    
    @objc func handleToggle() {
        dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrayofData.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! BrandsProTableViewCell
        cell.selectionStyle = .none
        let product = arrayofData[indexPath.row]
        cell.titleView.text = product.name
        cell.iconView.setImageFromURl(stringImageUrl: (arrayofData[indexPath.row].image_link)!)
        
        return cell
         
     }
     
  
    func ApiResponsegetArrived(_ Response:AnyObject, isType: String){
        viewActivity.hideActivityIndicator()
        if(isType == ActivityConstants.INT_BRAND){
            print("close kkkapp")
            if let detailsjson = Response as? [[String: AnyObject]] {
                for model in detailsjson {
                    if let brand = model["brand"] as? String  {
                        print(brand)
                        let model =  ProductModel(id: (model["id"] as? Int), brand: (model["brand"] as? String) ?? "", name: (model["name"] as? String) ?? "", price: (model["price"] as? String), price_sign: (model["price_sign"] as? String ?? ""), currency: (model["currency"] as? String) ?? "", image_link: (model["image_link"] as? String) ?? "", product_link: (model["product_link"] as? String ?? ""), website_link: (model["website_link"] as? String ?? ""), description: (model["description"] as? String ?? ""), rating: (model["rating"] as? String ?? ""), category: (model["category"] as? String ?? ""), product_type: (model["product_type"] as? String ?? ""), created_at: (model["created_at"] as? String ?? ""),updated_at: (model["updated_at"] as? String ?? ""), product_api_url: (model["product_api_url"] as? String ?? ""), api_featured_image: (model["api_featured_image"] as? String ?? "") )
                        arrayofData.append(model)
                    }
                }
                DispatchQueue.main.async {
                    self.productBrandTableView.rowHeight = 100
                    self.productBrandTableView.reloadData()
                }
            }
        }
    }
}
