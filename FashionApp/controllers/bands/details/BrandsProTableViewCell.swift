//
//  BrandsProTableViewCell.swift
//  FashionApp
//
//  Created by Manu Aravind on 16/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class BrandsProTableViewCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    
    @IBOutlet weak var titleView: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
