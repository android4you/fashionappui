//
//  UtilsConstant.swift
//  FashionApp
//
//  Created by Manu Aravind on 15/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

class UtilsConstant {
    internal static let BASE_URL : String = "http://makeup-api.herokuapp.com/api/v1/products.json?"
}
