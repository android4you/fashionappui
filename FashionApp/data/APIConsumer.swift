import Foundation
import UIKit

protocol APIConsumerDelegate {
    func APIResponseArrived(_ Response:AnyObject)
}

class APIConsumer {
    var delegate:APIConsumerDelegate! = nil
    
    
    
    func doRequestGet(_ url:String)  {
        print(url)
        let url = URL(string:url)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            do{
                
                if error != nil{
                    print("error")
                 //   print(error?.localizedDescription)
                    return
                }
                print("resoponse : \((response as?  HTTPURLResponse)?.statusCode)")
                if let res = response as?  HTTPURLResponse, res.statusCode == 200 {                    
                    
                    let jsonData:NSArray = (try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSArray)!
                    
                    DispatchQueue.main.async(execute: {
                        if self.delegate != nil {
                            self.delegate.APIResponseArrived(jsonData)
                        }
                    })
                }else{
                    print(response!.description)
                    return
                }
            }
            catch{
                print("Exception thrown")
            }
            
        }).resume()
    }
}
