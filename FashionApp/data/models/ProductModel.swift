//
//  ProductModel.swift
//  FashionApp
//
//  Created by Manu Aravind on 16/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
class ProductModel {
    var id: Int?
    var brand: String?
    var name: String = ""
    var price: String?
    var price_sign: String?
    var currency: String?
    var image_link: String?
    var product_link: String?
    var website_link: String?
    var description: String?
    var rating: String?
    var category: String?
    var product_type: String?
    var created_at: String?
    var updated_at: String?
    var product_api_url: String?
    var api_featured_image: String?
    
    init( id: Int?,
     brand: String?,
     name: String?,
     price: String?,
     price_sign: String?,
     currency: String?,
     image_link: String?,
     product_link: String?,
     website_link: String?,
     description: String?,
     rating: String?,
     category: String?,
     product_type: String?,
     created_at: String?,
     updated_at: String?,
     product_api_url: String?,
     api_featured_image: String?){
        
        self.id = id;
          self.brand = brand;
        self.name = name ?? "-";
          self.price = price;
          self.price_sign = price_sign;
          self.currency = currency;
          self.image_link = image_link;
          self.product_link = product_link;
          self.website_link = website_link;
          self.description = description;
          self.rating = rating;
          self.category = category;
          self.product_type = product_type;
          self.created_at = created_at;
          self.updated_at = updated_at;
          self.product_api_url = product_api_url;
          self.api_featured_image = api_featured_image;
        
    }
    
    
}
