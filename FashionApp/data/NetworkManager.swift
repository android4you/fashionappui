//
//  NetworkManager.swift
//  FashionApp
//
//  Created by Manu Aravind on 14/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

@objc protocol NetworkManagerDelegate {
    @objc optional func ApiResponsegetArrived(_ Response: AnyObject, isType: String)
}
class NetworkManager :  APIConsumerDelegate {
    var delegate:NetworkManagerDelegate!
    var objAPIConsumer : APIConsumer = APIConsumer()
    var restDelegate : NetworkManagerDelegate! = nil
    var token :String = String()
    var viewActivity = ProgressUtils()
    
    func initwith()->AnyObject{
        self.objAPIConsumer = APIConsumer()
        self.objAPIConsumer.delegate = self
        return self
        
    }
    func getProducts() {
        self.token = ActivityConstants.INT_PRODUCT
        self.objAPIConsumer.doRequestGet(UtilsConstant.BASE_URL)
    }
    
    func getProductByBrand(_brand: String){
        self.token = ActivityConstants.INT_BRAND
        //http://makeup-api.herokuapp.com/api/v1/products.json?
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "makeup-api.herokuapp.com"
        urlComponents.path = "/api/v1/products.json"
        urlComponents.queryItems = [
           URLQueryItem(name: "brand", value: _brand)
        ]

        print(urlComponents.url?.absoluteString) 
        
        self.objAPIConsumer.doRequestGet(urlComponents.url?.absoluteString ?? "")
    }
    
    
    func APIResponseArrived(_ Response:AnyObject){
        print("mmmmmmmmm")
        restDelegate.ApiResponsegetArrived!(Response, isType: token)
        viewActivity.hideActivityIndicator()
    }
}
