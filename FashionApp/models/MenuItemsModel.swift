//
//  MenuItemsModel.swift
//  FashionApp
//
//  Created by Manu Aravind on 14/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

class MenuItemsModel {
    
    var id : Int?
       var title: String?

       init(id: Int, title: String) {
                self.id = id
                self.title = title
            }
}
