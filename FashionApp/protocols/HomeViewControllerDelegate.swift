//
//  HomeViewControllerDelegate.swift
//  FashionApp
//
//  Created by Manu Aravind on 14/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

protocol HomeViewControllerDelegate {
    
    func handleMenuToggle(index: Int)
}

