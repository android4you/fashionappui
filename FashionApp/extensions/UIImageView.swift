//
//  UIImageView.swift
//  FashionApp
//
//  Created by Manu Aravind on 16/03/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

extension UIImageView {
    func setImageFromURl(stringImageUrl url: String){
        if let url = NSURL(string: url) {
            if let data = NSData(contentsOf: url as URL) {
                DispatchQueue.main.async {
                    self.image = UIImage(data: data as Data)
                }
            }
        }
    }
}
